import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindYourCarNowComponent } from './find-your-car-now.component';

describe('FindYourCarNowComponent', () => {
  let component: FindYourCarNowComponent;
  let fixture: ComponentFixture<FindYourCarNowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindYourCarNowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindYourCarNowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
