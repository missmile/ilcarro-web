import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import {
  InputTextModule,
  ButtonModule,
  Toolbar,
  TabViewModule,
  CalendarModule,
  SliderModule
} from "primeng";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { RegistrationComponent } from "./registration/registration.component";
import { MainRegistrationComponent } from "./main-registration/main-registration.component";
import { FeedbacksComponent } from "./feedbacks/feedbacks.component";
import { JoinNowComponent } from "./join-now/join-now.component";
import { ThreeMostPopularComponent } from "./three-most-popular/three-most-popular.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FindYourCarNowComponent } from "./find-your-car-now/find-your-car-now.component";
import { LogInComponent } from "./footer/log-in/log-in.component";
import { TabSearchComponent } from "./footer/tab-search/tab-search.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    RegistrationComponent,
    MainRegistrationComponent,
    FeedbacksComponent,
    JoinNowComponent,
    Toolbar,
    ThreeMostPopularComponent,
    FindYourCarNowComponent,
    LogInComponent,
    TabSearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InputTextModule,
    ButtonModule,
    FontAwesomeModule,
    TabViewModule,
    CalendarModule,
    SliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
