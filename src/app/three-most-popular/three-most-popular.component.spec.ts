import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreeMostPopularComponent } from './three-most-popular.component';

describe('ThreeMostPopularComponent', () => {
  let component: ThreeMostPopularComponent;
  let fixture: ComponentFixture<ThreeMostPopularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreeMostPopularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreeMostPopularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
